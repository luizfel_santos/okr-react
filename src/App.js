import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import './App.css';

import Header from './components/Header';
import WordContainer from './components/WordContainer';
import { Switch, Route } from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="App row flex-xl-nowrap">
          <Header />

          <main className="col-12 col-sm-9 col-xl-10 offset-sm-3 offset-xl-2 pt-3 pb-3">
            <div className="row justify-content-center py-3 px-3">
              <Switch>
                <Route exact path='/' component={WordContainer} />
                <Route path='/:tense' component={WordContainer} />
              </Switch>
            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default App;
