import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Form from './Form';
import ContainerTitle from './ContainerTitle';
import Results from './Results';
import FinalResults from './FinalResults';
import dataService from '../services/data-service';

function wordDataReducer(wordData) {
  const wordKey = Object.keys(wordData)[0];

  return {
    verb: wordKey,
    translation: wordData[wordKey]['translation'],
    answers: wordData[wordKey]['answers']
  };
}

class WordContainer extends Component {
  constructor(props) {
    super(props);

    this.resetData = this.resetData.bind(this);
    this.checkAnswers = this.checkAnswers.bind(this);
    this.goToNext = this.goToNext.bind(this);
    this.state = {
      showResults: false,
      showFinalResults: false,
      index: 0,
      items: [],
      wordData: {},
      correctCount: 0,
      wrongCount: 0
    };
  }

  resetData() {
    this.setState({
      showResults: false,
      showFinalResults: false,
      index: 0,
      wordData: wordDataReducer(this.state.items[0]),
      correctCount: 0,
      wrongCount: 0
    });
  }

  componentWillReceiveProps(nextProps) {
    let path;

    if (nextProps.match.path == '/') path = 'present';
    else path = nextProps.match.params.tense;

    this.loadData(path);
  }

  loadData(verbTense) {
    dataService(verbTense).end((err, res) => {
      if (err) {
        this.setState({
          err
        });
      } else {
        const data = JSON.parse(res.text);
        const items = data['items'];
        const wordData = items[0];

        this.setState({
          showResults: false,
          showFinalResults: false,
          index: 0,
          path: verbTense,
          verbTense: data['verb-tense'],
          items: items,
          wordData: wordDataReducer(wordData),
          correctCount: 0,
          wrongCount: 0
        });
      }
    });
  }

  checkAnswers(values) {
    this.setState({
      showResults: true,
      values
    });
  }

  goToNext(stepResults, goForward = true) {
    let correctCount = this.state.correctCount + stepResults.correctCount;
    let wrongCount = this.state.wrongCount + stepResults.wrongCount;

    if (!goForward) {
      this.setState({
        showFinalResults: true,
        correctCount,
        wrongCount
      });

      return;
    }

    let index = this.state.index;
    index++;

    this.setState({
      showResults: false,
      wordData: wordDataReducer(this.state.items[index]),
      index: index,
      correctCount,
      wrongCount
    });
  }

  setWordContent() {
    if (this.state.showFinalResults) {
      return <FinalResults data={this.state} resetData={this.resetData} />;
    }

    if (!this.state.showResults) {
      return <Form onSubmit={this.checkAnswers} />;
    } else {
      return (
        <div className="results">
          <p className="results-title">Result:</p>
          <Results data={this.state} goToNext={this.goToNext} />
        </div>
      );
    }
  }

  componentDidMount() {
    let path;
    
    if (this.props.match.path == '/') path = 'present';
    else path = this.props.match.params.tense;
    
    this.loadData(path);
  }

  render() {
    const element = this.setWordContent();

    let title;

    if (this.state.showFinalResults) title = <h2>Finals results</h2>;
    else title = <ContainerTitle wordData={this.state.wordData} />;

    return (
      <div className="col-12 col-lg-10 border py-3 px-3">
        <h1>{this.state.verbTense}</h1>
        {title}
        {element}
      </div>
    );
  }
}

WordContainer.propTypes = {
  match: PropTypes.object
};

export default WordContainer;
