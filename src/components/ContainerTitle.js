import React, { Component } from 'react';
import PropTypes from 'prop-types';
class ContainerTitle extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const data = this.props.wordData;
    if (data != undefined) {
      return (
        <div>
          <h3>{data.verb}</h3>
          <p>{data.translation}</p>
          <i>{'Note: If the verb being conjugated has any sort of contraction (e.g. j\'ai), you should type the whole conjugation (i.e. "j\'ai" instead of just "ai", and so on)'}</i>

          <hr />
        </div>
      );
    } else {
      return null;
    }
  }
}

ContainerTitle.propTypes = {
  wordData: PropTypes.object
};

export default ContainerTitle;
