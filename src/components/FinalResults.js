import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
class FinalResults extends Component {
  constructor(props) {
    super(props);

    this.state = {
      path: '',
      stats: {}
    };
  }

  componentDidMount() {
    const correctCount = this.props.data.correctCount;
    const wrongCount = this.props.data.wrongCount;

    this.setState({
      path: (this.props.data.path == 'present') ? '' : this.props.data.path,
      stats: {
        percentage: (correctCount * 100) / (correctCount + wrongCount),
        correctCount: correctCount,
        wrongCount: wrongCount
      }
    });
  }

  render() {
    if (this.state.stats.percentage == undefined) return null;

    return (
      <div>
        <p>You got {this.state.stats.percentage.toFixed(2)}% of the answears right</p>
        <p>Correct answears: {this.state.stats.correctCount}</p>
        <p>Wrong answears: {this.state.stats.wrongCount}</p>

        <button type="button" className="btn btn-primary" onClick={this.props.resetData}>Start over</button>
      </div>
    );
  }
}

FinalResults.propTypes = {
  data: PropTypes.object,
  resetData: PropTypes.func
};

export default FinalResults;
