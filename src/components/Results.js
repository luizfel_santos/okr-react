import React, { Component } from 'react';
import PropTypes from 'prop-types';

function isAnswerCorrect(answer, value) {
  const answers = answer.split('/');
  return answers.some((el) => el.toLowerCase() == value.toLowerCase().trim());
}

function checkAnswers(answers, values) {
  const pronouns = ['je', 'tu', 'il/elle', 'nous', 'vous', 'ils/elles'];
  let results = [];

  Object.keys(answers).map((key, i) => {
    results.push({
      label: pronouns[i],
      correctAnswer: answers[key],
      userAnswer: (values[i]) ? values[i] : 'no input',
      status: (isAnswerCorrect(answers[key], values[i])) ? 'correct' : 'wrong'
    });
  });

  return results;
}
class ResultItems extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const items = this.props.fields.map((item, index) => (
      <div className="row" key={index}>
        <span className="col-12 col-sm-3"><b>{item.label}</b></span>
        <span className="col-12 col-sm-3">{item.userAnswer}</span>
        <span className={'col-12 col-sm-3 text-' + (item.status == 'correct' ? 'success' : 'danger')}>
          <i className={'fas pr-2 fa-' + (item.status == 'correct' ? 'check' : 'times')}></i>{item.status}
        </span>
        <span className="col-12 col-sm-3">{item.correctAnswer}</span>
      </div>
    ));

    return <div className="mb-3">{items}</div>;
  }
}

ResultItems.propTypes = {
  fields: PropTypes.array
};
class Results extends Component {
  constructor(props) {
    super(props);

    this.finishStep = this.finishStep.bind(this);
    const data = this.props.data;
    this.state = {
      fields: checkAnswers(data.wordData.answers, data.values)
    };
  }

  finishStep() {
    let correctCount = 0;
    let wrongCount = 0;

    this.state.fields.map((item) => {
      if (item.status == 'correct') correctCount++;
      else wrongCount++;
    });

    const stepResults = {
      correctCount, wrongCount
    };
    const goForward = this.props.data.items.length - 1 == this.props.data.index;

    this.props.goToNext(stepResults, !goForward);
  }

  render() {
    if (this.props.data === undefined) {
      return <p>Loading</p>;
    }
    
    let buttonText;
    
    if (this.props.data.items.length - 1 == this.props.data.index) buttonText = 'View results';
    else buttonText = 'Conjugate next verb';

    return (
      <div>
        <div className="row">
          <span className="col-12 col-sm-3"></span>
          <span className="col-12 col-sm-3">your answer</span>
          <span className="col-12 col-sm-3"></span>
          <span className="col-12 col-sm-3">correct answer</span>
        </div>
        <ResultItems fields={this.state.fields} />
        <button type="button" onClick={this.finishStep} className="btn btn-primary">
          {buttonText}
        </button>
      </div>
    );
  }
}

Results.propTypes = {
  data: PropTypes.object,
  goToNext: PropTypes.func
};

export default Results;
