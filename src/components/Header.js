import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
class Menu extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const items = this.props.items.map((item, index) => (
      <li className="nav-item" key={index}>
        <Link to={item.path}>{item.label}</Link>
      </li>
    ));

    return (
      <nav className="mt-sm-4">
        <ul className="nav flex-column">{items}</ul>
      </nav>
    );
  }
}

Menu.propTypes = {
  items: PropTypes.array
};
class Header extends Component {
  constructor(props) {
    super(props);

    this.items = [
      { label: 'Présent', path: '' },
      { label: 'Passé simple', path: 'past-simple' }
    ];
  }

  render() {
    return (
      <header className="App-header col-12 col-sm-3 col-xl-2 sidebar">
        <h1 className="App-title">French verbs conjugation</h1>
        <Menu items={this.items} />
      </header>
    );
  }
}

export default Header;
