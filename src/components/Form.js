import React, { Component } from 'react';
import PropTypes from 'prop-types';
class FormFields extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const items = this.props.fields.map((item, index) => (
      <div className="form-group row" key={index}>
        <label htmlFor={index} className="col-12 col-sm-2 col-form-label">
          {item.label}
        </label>
        <div className="col-12 col-sm-10">
          <input type="text" id={index} className="form-control" />
        </div>
      </div>
    ));

    return items;
  }
}

FormFields.propTypes = {
  fields: PropTypes.array
};
class Form extends Component {
  constructor(props) {
    super(props);

    this.fields = [
      { label: 'je' },
      { label: 'tu' },
      { label: 'il/elle' },
      { label: 'nous' },
      { label: 'vous' },
      { label: 'ils/elles' },
    ];

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    var inputs = document.getElementsByTagName('input');
    var values = [];
    for (var i = 0; i < inputs.length; i++) {
      values.push(inputs[i].value);
    }

    this.props.onSubmit(values);

    e.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <FormFields fields={this.fields} />

        <button type="submit" className="btn btn-primary">
          Check answers
        </button>
      </form>
    );
  }
}

Form.propTypes = {
  onSubmit: PropTypes.func
};

export default Form;
