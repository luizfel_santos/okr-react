import { applyMiddleware, createStore } from 'redux';

import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
// import promise from 'redux-promise-middleware';

import reducer from './reducers/index';
import dataService from './services/data-service';

/*const logger = (store) => (next) => (action) => {
	console.log('action fired', action);
}*/

const logger = createLogger({
  // ...options
});

const middleware = applyMiddleware(logger, thunk, dataService);

const store = createStore(reducer, middleware);

export default store;
