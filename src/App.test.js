import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { Provider } from 'react-redux';
import store from './store';

import axios from 'axios';

describe('all tests', () => {

  it('random test', () => {
      expect(1).toBe(1);
  });

  it('is prop from remote data false?', () => {
      expect.assertions(1);

      return axios.get('https://api.github.com/users/robconery')
          .then(response => response.data)
          .then(result => {
              console.log(result);
              expect(result.site_admin).toBeTruthy();
              //done();
          });

  });

  function sum(a, b) {
    return a + b;
  }

  function showConsoleText(text, times, callback) {
    for(let index = 0; index < times; index++) {
      callback();
    }
  }

  const m = jest.fn();
  showConsoleText('essa é a chamada número ', 3, m);
  it('check callback', () => {
    expect(m.mock.calls.length).toBe(3);
  });

  it('1 + 2 equals 3', () => {
    expect(sum(1,2)).toBe(3);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}><App /></Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

});