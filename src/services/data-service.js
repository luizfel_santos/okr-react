import request from 'superagent';

const host = 'http://localhost/experiments/react/deutsch-verben';
const dataService = (verbTense) => {
    return request
        .get(host + '/src/data/' + verbTense +'.json');

};

export default dataService;